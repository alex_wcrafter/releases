#!/bin/bash
export GITHUB_USER="alexwcrafter"
export GITHUB_EMAIL="kontakt@ayokaacr.de"

export device="teos"
export ROM="LineageOS"
export ROM_DIR="/home/appveyor/build/rom"
export ROM_VERSION="14"
export official="false"
export local_manifest_url="https://github.com/windowzytch/platform_manifest/raw/cm-14.1/teos_staging.xml"
export manifest_url="https://github.com/LineageOS/android"
export rom_vendor_name="lineage"
export branch="cm-14.1"
export bacon="bacon"
export buildtype="userdebug"
export clean="installclean"
export generate_incremental=""
export upload_recovery="false"
export ccache="false"
export ccache_size=""

export jenkins="false"

export release_repo="alexwcrafter/releases"

export timezone="UTC"
